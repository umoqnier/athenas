import requests
import json


def get_scopus_data(key_word, api_key, field):
    url = ('https://api.elsevier.com/content/search/scopus?query='+field+'('+key_word+')&apiKey='+api_key)
    try:
        resp = requests.get(url, headers={'Accept':'application/json', 'X-ELS-APIKey':api_key})
    except:
        print("CONNECTION ERROR")

    print("Right lml")
    return json.loads(resp.text.encode('utf-8'))

def main():
    api_key = 'b3a71de2bde04544495881ed9d2f9c5b'
    field = input("Field: ")
    key_word = input("Some Key: ")
    try:
        data = get_scopus_data(key_word, api_key, field)
        with open('data.txt', 'w') as outfile:
            json.dump(data, outfile, indent=4)
    except:
        print("WRITE FILE ERROR")


if __name__ == '__main__':
    main()
